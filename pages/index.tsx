import Image from 'next/image'
import { Inter } from 'next/font/google'
import HomePageView from '@/src/view/HomePageView'
import FooterView from '@/src/view/FooterView'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
   <HomePageView />
   
   <FooterView />
   </>
  )
}
