import Link from 'next/link'
import React from 'react'

function Footer() {
  return (
    <div className='bg-black flex-col p-4'>
      <div className='p-4'>
        <ul>
          <li className='text-white uppercase'>
            <Link href={'/'}>
              <span>
                {`Carte cadeau`}
              </span>
            </Link>
          </li>
          <li className='text-white uppercase'>
            <Link href={'/'}>
              <span>
                {`Trouver un magasin`}
              </span>
            </Link>
          </li>
          <li className='text-white uppercase'>
            <Link href={'/'}>
              <span>
                {`Nike journal`}
              </span>
            </Link>
          </li>
          <li className='text-white uppercase'>
            <Link href={'/'}>
              <span className=''>
                {`Devenir membre`}
              </span>
            </Link>
          </li>
          <li className='text-white uppercase'>
            <Link href={'/'}>
              <span>
                {`Réduction pour étudiants`}
              </span>
            </Link>
          </li>
          <li className='text-white uppercase'>
            <Link href={'/'}>
              <span>
                {`Commentaires`}
              </span>
            </Link>
          </li>
          <li className='text-white uppercase'>
            <Link href={'/'}>
              <span>
                {`Code promo`}
              </span>
            </Link>
          </li>
          
        </ul>

      </div>
    </div>
  )
}

export default Footer
