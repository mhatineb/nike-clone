import Link from 'next/link'
import React from 'react'
import {MagnifyingGlassIcon} from '@heroicons/react/16/solid'

function Header() {
  return (
    <header>
                <div className='w-full px-12 py-4 flex flex-row items-center justify-between'>
                    <div
                        className='w-7 h-7 bg-black rounded-full items-center'
                    >

                    </div>
                    <nav>
                        <ul
                            className='flex space-x-3 items-center'
                        >
                            <li
                                className='pl-3 text-md font-medium'
                            >
                                <Link
                                    href={'/'}
                                    className='hover:text-[#757575] hover:underline'
                                >
                                    <span>
                                        {`Nouveauté`}
                                    </span>
                                </Link>
                            </li>
                            <li
                                className='pl-3 text-md font-medium'
                            >
                                <Link
                                    href={'/'}
                                    className='hover:text-[#757575] hover:underline'
                                >
                                    <span>
                                        {`Homme`}
                                    </span>
                                </Link>
                            </li>
                            <li
                                className='pl-3 text-md font-medium'
                            >
                                <Link
                                    href={'/'}
                                    className='hover:text-[#757575] hover:underline'
                                >
                                    <span>
                                        {`Femme`}
                                    </span>
                                </Link>
                            </li>
                            <li
                                className='pl-3 text-md font-medium'
                            >
                                <Link
                                    href={'/'}
                                    className='hover:text-[#757575] hover:underline'
                                >
                                    <span>
                                        {`Enfant`}
                                    </span>
                                </Link>

                            </li>
                            <li className='pl-3 text-md font-medium'>
                                <Link
                                    href={'/'}
                                    className='hover:text-[#757575] hover:underline'
                                >
                                    <span>
                                        {`Offres`}
                                    </span>
                                </Link>
                            </li>
                        </ul>

                    </nav>
                    
                    <div className='flex flex-row items-center px-2'>

                        <div className="pt-1 pr-2 relative  text-gray-600 flex flex-row">
                            <input className="border-2 bg-black bg-opacity-5 h-10 px-10 pr-16 rounded-xl text-sm focus:outline-none"
                                type="search" name="search" placeholder="Rechercher" />
                            <button type="submit" className="absolute left-0 px-1 h-10 w-16">
                                
                                <MagnifyingGlassIcon className='w-7'/>
                            </button>




                        </div>
                        <div className='space-x-3 flex flex-row'>
                            <div
                                className='w-7 h-7 bg-black rounded-full items-center px-3'
                            >

                            </div>
                            <div
                                className='w-7 h-7 bg-black rounded-full items-center'
                            >

                            </div>
                        </div>
                    </div>

                </div>

            </header>
  )
}

export default Header
